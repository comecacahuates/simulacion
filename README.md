# Protocolo de enrutamiento para VANETs con nodos autoconfigurables #

Simulación de OMNeT++ del protoclo de enrutamiento.

## Requerimientos ##

- OMNeT++ 6.0 (ver <https://omnetpp.org/>)
- SUMO 1.8 (ver <https://www.eclipse.org/sumo/>)
- INET 4.3 (ver <https://inet.omnetpp.org/>)
- Veins 5.0 (ver <https://veins.car2x.org/>)
